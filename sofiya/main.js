let slider_img=document.getElementById("slider-img");
let images = ['travel.jpg', 'sea.jpeg', 'lake.jpeg', 'mountain.jpeg'];
let i = 0; //Current Image Index
function prev() {
    if (i <= 0) i = images.length;
    i--;
    return setImg()
}

function next() {
    if (i >= images.length - 1) i = -1;
    i++;
    return setImg();
}
function setImg() {
    return slider_img.setAttribute('src', 'images/' + images[i]);
}
function revealMessage() {
    document.getElementById("hiddenMessage").style.display = "block";
}
$(document).ready(function () {
    $("#demobutton").click(function () {
        $("#demobutton").hide();
    }); 
});

