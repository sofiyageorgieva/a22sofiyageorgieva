Meta:
@logInForum

Narrative:
As a user
I want to logIn in the forum
so that I can create a new Topic

Scenario: Create New Topic In the Forum
When Click forum.LogInButton element
And Type sofiya.georgieva.test@abv.bg in forum.EmailButton field
And Type testtest123 in forum.PasswordButton field
Then Click forum.SignInButton element

