Meta:
@createTopic

Narrative:
As a registered user
I want to create a new topic
So that I can join the discussions in the forum

Scenario: Create New Topic In the Forum
When Click forum.NewTopic element
And Type test in forum.Title field
And Type I am creating test topic in forum.Message field
Then Click forum.CreateTopic element
