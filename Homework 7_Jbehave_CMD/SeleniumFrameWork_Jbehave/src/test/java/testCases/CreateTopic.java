package testCases;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CreateTopic extends BaseTest {
    private static final String telerikForumUrl = "https://stage-forum.telerikacademy.com";
    private WebDriver driver;

    @BeforeClass
    public static void classInit() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\chromedriver_win32\\chromedriver.exe");
    }

    @Before
    public void testInit() {
        driver = new ChromeDriver();
        driver.get(telerikForumUrl);
        driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
        WebElement logInButton = driver.findElement(By.xpath("//span[@class='d-button-label']"));
        logInButton.click();

        WebElement emailButton = driver.findElement(By.id("Email"));
        emailButton.sendKeys("sofiya.georgieva.test@abv.bg");

        WebElement passwordButton = driver.findElement(By.id("Password"));
        passwordButton.sendKeys("testtest123");

        WebElement signInButton = driver.findElement(By.id("next"));
        signInButton.click();
    }
    @Test
    public void telerikForumHomePageNavigated() {
        String expectedHomePageUrl = "https://stage-forum.telerikacademy.com/";
        Assert.assertEquals("Page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void createNewTopic() {

        WebElement newTopic = driver.findElement(By.id("create-topic"));
        newTopic.click();
        WebElement title = driver.findElement(By.id("reply-title"));
        title.sendKeys("test topic");
        WebElement message = driver.findElement(By.xpath("//textarea[contains(@placeholder,'Type here')]"));
        message.sendKeys("I am creating test topic");
        WebElement createTopic = driver.findElement(By.xpath("//button[@title='Or press Ctrl+Enter']"));
        createTopic.click();
    }
    @After
    public void logOutFromTheForum() {
        WebElement avatar = driver.findElement(By.xpath("//a[@title='Sofiya Georgieva Test']//img[@class='avatar']"));
        avatar.click();

        WebElement icon = driver.findElement(By.xpath("//a[@title='sofiya.georgieva.tes']"));
        icon.click();

        WebElement logOut = driver.findElement(By.xpath("//ul/li[@class='logout read']"));
        logOut.click();

    }
}

